import 'dart:convert';

import 'package:sample_news_app/model/news.dart';
import 'package:http/http.dart' as http;

class APIService {
  List<News> newsFromJson(String jsonData) {
    final data = json.decode(jsonData);
    return List<News>.from(data.map((item) => News.fromJson(item)));
  }

  Future<List<News>> getListNews() async {
    final response = await http.get(
        'https://60a4954bfbd48100179dc49d.mockapi.io/api/innocent/newsapp/articles');
    try {
      if (response.statusCode == 200) {
        return newsFromJson(response.body);
      } else {
        print('error:' + response.body);
        return [];
      }
    } catch (e) {
      print(e);
      print(response.statusCode);
      return [];
    }
  }


}
