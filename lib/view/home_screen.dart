import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:sample_news_app/model/news.dart';
import 'package:sample_news_app/model/param_news.dart';
import 'package:sample_news_app/view/detail_news_screen.dart';
import 'package:sample_news_app/view_model/api_service.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/homeScreen';
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var apiServices = APIService();
  List<News> listNews;
  int indicator;
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    apiServices.getListNews().then((value) {
      setState(() {
        listNews = value;
      });
    });
  }

  String removeAfterString(String string, String remove) {
    String result = string.substring(0, string.indexOf(remove));
    // print(result);
    return result;
  }

  String formatingDate(String date) {
    String dateFormat = DateFormat('EEEE MM yyyy').format(DateTime.parse(date));
    return dateFormat;
  }

 String convertToTitleCase(String text) {
  if (text == null) {
    return null;
  }

  if (text.length <= 1) {
    return text.toUpperCase();
  }

  // Split string into multiple words
  final List<String> words = text.split(' ');

  // Capitalize first letter of each words
  final capitalizedWords = words.map((word) {
    if (word.trim().isNotEmpty) {
      final String firstLetter = word.trim().substring(0, 1).toUpperCase();
      final String remainingLetters = word.trim().substring(1);

      return '$firstLetter$remainingLetters';
    }
    return '';
  });

  // Join/Merge all words back to one String
  return capitalizedWords.join(' ');
}

  

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    Widget _headLineNews(News item) {
      return Container(
        // color: Colors.grey,
        margin: EdgeInsets.all(8.0),
        height: height * 0.40,
        child: Column(
          children: <Widget>[
            Expanded(
                flex: 7,
                child: Container(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              item.contentThumbnail,
                            ))),
                  ),
                )),
            Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.centerLeft,
                  // color: Colors.teal,
                  child: Text(
                    'General',
                    style: TextStyle(color: Colors.pinkAccent),
                  ),
                )),
            Expanded(
                flex: 2,
                child: Container(
                    alignment: Alignment.topLeft,
                    // color: Colors.lightGreen,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, DetailNewsScreen.routeName,
                            arguments: ParamNews(
                                item.id,
                                formatingDate(item.createdAt),
                                item.contributorName,
                                item.contributorAvatar,
                                convertToTitleCase(item.title),
                                item.content,
                                item.contentThumbnail,
                                item.slideshow));
                      },
                      child: Text(
                        convertToTitleCase(item.title),
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ))),
          ],
        ),
      );
    }

    Widget _headerLatestNews() {
      return Container(
        width: width,
        height: height * 0.1,
        alignment: Alignment.center,
        child: Text(
          'LATEST NEWS',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      );
    }

    Widget _indicatorheadLineNews() {
      return Padding(
        padding: EdgeInsets.fromLTRB(100, 0, 100, 0),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Container(
                    width: width,
                    height: height * 0.1,
                    alignment: Alignment.center,
                    child: indicator == 0
                        ? Divider(color: Colors.pinkAccent)
                        : Divider(color: Colors.grey)),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Container(
                    width: width,
                    height: height * 0.1,
                    alignment: Alignment.center,
                    child: indicator == 1
                        ? Divider(color: Colors.pinkAccent)
                        : Divider(color: Colors.grey)),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Container(
                    width: width,
                    height: height * 0.1,
                    alignment: Alignment.center,
                    child: indicator == 2
                        ? Divider(color: Colors.pinkAccent)
                        : Divider(color: Colors.grey)),
              ),
            ),
          ],
        ),
      );
    }

    Widget _latestNews(News item) {
      // print(item.id);
      return Container(
        margin: EdgeInsets.only(top: 15),
        width: width,
        height: height * 0.5,

        child: Column(
          children: <Widget>[
            Expanded(
                flex: 6,
                child: Container(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(item.contentThumbnail))),
                  ),
                )),
            Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.centerLeft,
                  // color: Colors.teal,
                  child: Text(
                    'General',
                    style: TextStyle(color: Colors.pinkAccent),
                  ),
                )),
            Expanded(
                flex: 2,
                child: Container(
                    alignment: Alignment.topLeft,
                    // color: Colors.lightGreen,
                    child: GestureDetector(
                      onTap: () {
                        print('tekan');
                        Navigator.pushNamed(context, DetailNewsScreen.routeName,
                            arguments: ParamNews(
                                item.id,
                                item.createdAt,
                                item.contributorName,
                                item.contributorAvatar,
                                item.title,
                                item.content,
                                item.contentThumbnail,
                                item.slideshow));
                      },
                      child: Text(
                       convertToTitleCase(item.title),
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ))),
            Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    formatingDate(removeAfterString(item.createdAt, 'T')),
                    style: TextStyle(fontSize: 8),
                  ),
                ))
          ],
        ),
        // ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        flexibleSpace: Container(
          child: Center(
            child: Container(
              padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
              // color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.fromLTRB(5, 5, 35, 5),
                    child: Image.asset('assets/images/logo.png'),
                  )),
                  Expanded(
                      child: Text(
                    'General Tag',
                    textAlign: TextAlign.end,
                    style: TextStyle(color: Colors.pinkAccent),
                  ))
                ],
              ),
            ),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
        child: ListView(
          children: <Widget>[
            CarouselSlider(
                height: height * 0.40,
                // aspectRatio: 2,
                viewportFraction: 1.0,
                initialPage: 0,
                // enableInfiniteScroll: true,
                // reverse: false,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                // autoPlayCurve: Curves.fastOutSlowIn,
                // scrollDirection: Axis.horizontal,
                items: listNews?.isEmpty ?? true
                    ? [1].map((i) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          },
                        );
                      }).toList()
                    : listNews.asMap().entries.map((item) {
                        int idx = item.key;
                        News value = item.value;
                        print(idx);
                          return Builder(
                            builder: (BuildContext context) {
                              return _headLineNews(value);
                            },
                          );
                      }).toList()),
            _indicatorheadLineNews(),
            _headerLatestNews(),
            if (listNews?.isEmpty ?? true)
              Center(
                child: CircularProgressIndicator(),
              )
            else
              for (var item in listNews)
                if (item.id != '1') _latestNews(item)
          ],
        ),
      ),
    );
  }
}


