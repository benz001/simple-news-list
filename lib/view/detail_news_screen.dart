import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sample_news_app/model/param_news.dart';

class DetailNewsScreen extends StatefulWidget {
  static const routeName = '/detailNewsScreen';
  const DetailNewsScreen({Key key}) : super(key: key);

  @override
  _DetailNewsScreenState createState() => _DetailNewsScreenState();
}

class _DetailNewsScreenState extends State<DetailNewsScreen> {
  String capitalize(String string) {
    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    final ParamNews param = ModalRoute.of(context).settings.arguments;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Widget _titleNews() {
      return Container(
        alignment: Alignment.topLeft,
        child: Text(
          capitalize(param.title),
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
      );
    }

    Widget _authorNews() {
      return Container(
        alignment: Alignment.topLeft,
        child: Text(
          'By ' + param.contributorName,
          style: TextStyle(fontSize: 12, color: Colors.pinkAccent),
        ),
      );
    }

    Widget _datePostingNews() {
      return Container(
        alignment: Alignment.topLeft,
        child: Text(
          param.createdAt,
          style: TextStyle(fontSize: 12),
        ),
      );
    }

    Widget _imageNews() {
      // for (var item in param.slideshow) {
      //   print(item.toString());
      // }
      List<dynamic> slideShow = param.slideshow;
      return Container(
        height: 300,
        // color: Colors.grey,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 7,
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    image: DecorationImage(
                        image: NetworkImage(param.contentThumbnail),
                        fit: BoxFit.fitWidth)),
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                width: width,
                // color: Colors.yellow,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: slideShow.length,
                  itemBuilder: (BuildContext context, int index) {
                    print(slideShow[index]);
                    return Padding(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: Container(
                        width: 120,
                        height: 30,
                        margin: EdgeInsets.only(left: 10),
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            image: DecorationImage(
                                fit: BoxFit.fitWidth,
                                image: NetworkImage(slideShow[index]))),
                      ),
                    );
                  },
                  // children: <Widget>[
                  //   Padding(
                  //     padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  //     child: Container(
                  //       width: 120,
                  //       height: 30,
                  //       margin: EdgeInsets.only(left: 10),
                  //       color: Colors.grey,
                  //     ),
                  //   ),
                  //   Padding(
                  //     padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  //     child: Container(
                  //       width: 120,
                  //       height: 30,
                  //       margin: EdgeInsets.only(left: 10),
                  //       color: Colors.grey,
                  //     ),
                  //   ),
                  //   Padding(
                  //     padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  //     child: Container(
                  //       width: 120,
                  //       height: 30,
                  //       margin: EdgeInsets.only(left: 10),
                  //       color: Colors.grey,
                  //     ),
                  //   )
                  // ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                alignment: Alignment.topLeft,
                // color: Colors.purple,
                child: Text('Source:'),
              ),
            ),
          ],
        ),
      );
    }

    Widget _contentNews() {
      return Container(
        alignment: Alignment.topLeft,
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        // height: height * 0.05,}
        // color: Colors.green,
        child: Text(
          param.content,
          style: TextStyle(wordSpacing: 2, height: 2),
        ),
      );
    }

    Widget _recommendedNews() {
      return Container(
        alignment: Alignment.topLeft,
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        // height: height * 0.05,
        // color: Colors.lime,
        child: Row(
          children: [
            Text('Baca juga:'),
            SizedBox(
              width: 5,
            ),
            Text(
              'Lorem Ipsum',
              style: TextStyle(color: Colors.pinkAccent),
            ),
          ],
        ),
      );
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          flexibleSpace: Container(
            child: Center(
              child: Container(
                padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
                // color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                      padding: EdgeInsets.fromLTRB(5, 5, 35, 5),
                      child: Image.asset('assets/images/logo.png'),
                    )),
                    Expanded(
                        child: Text(
                      'General Tag',
                      textAlign: TextAlign.end,
                      style: TextStyle(color: Colors.pinkAccent),
                    ))
                  ],
                ),
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 15,
                ),
                _titleNews(),
                SizedBox(
                  height: 5,
                ),
                _authorNews(),
                SizedBox(
                  height: 5,
                ),
                _datePostingNews(),
                SizedBox(
                  height: 15,
                ),
                _imageNews(),
                SizedBox(
                  height: 15,
                ),
                _contentNews(),
                SizedBox(
                  height: 15,
                ),
                _recommendedNews(),
              ],
            ),
          ),
        ));
  }
}
