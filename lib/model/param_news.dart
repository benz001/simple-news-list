class ParamNews {
  String id;
  String createdAt;
  String contributorName;
  String contributorAvatar;
  String title;
  String content;
  String contentThumbnail;
  List<dynamic> slideshow;
  ParamNews(
      this.id,
      this.createdAt,
      this.contributorName,
      this.contributorAvatar,
      this.title,
      this.content,
      this.contentThumbnail,
      this.slideshow);
}
