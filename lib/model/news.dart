import 'dart:convert';

class News {
  String id;
  String createdAt;
  String contributorName;
  String contributorAvatar;
  String title;
  String content;
  String contentThumbnail;
  List<dynamic> slideshow;
  News(
      {this.id,
      this.createdAt,
      this.contributorName,
      this.contributorAvatar,
      this.title,
      this.content,
      this.contentThumbnail,
      this.slideshow});

  factory News.fromJson(Map<String, dynamic> json) {
    return News(
        id: json['id'],
        createdAt: json['createdAt'],
        contributorName: json['contributorName'],
        contributorAvatar: json['contributorAvatar'],
        title: json['title'],
        content: json['content'],
        contentThumbnail: json['contentThumbnail'],
        slideshow: json['slideshow']);
  }

  @override
  String toString() {
    return 'News{id: $id, createdAt: $createdAt, contributoeName: $contributorName, contributorAvatar: $contributorAvatar, title: $title, content: $content, contentThumbnail: $contentThumbnail, slideshow: $slideshow}';
  }


}
