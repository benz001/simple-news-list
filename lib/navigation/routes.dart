import 'package:flutter/material.dart';
import 'package:sample_news_app/view/detail_news_screen.dart';
import 'package:sample_news_app/view/example_carousel_screen.dart';
import 'package:sample_news_app/view/home_screen.dart';

class Routes extends StatelessWidget {
  const Routes({ Key key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/homeScreen',
      title: 'News App',
      routes: {
        HomeScreen.routeName: (context) => HomeScreen(),
        DetailNewsScreen.routeName: (context) => DetailNewsScreen(),
        ExampleCarouselScreen.routeName: (context) => ExampleCarouselScreen()
      },
    );
  }
}